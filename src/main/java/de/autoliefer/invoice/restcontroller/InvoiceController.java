package de.autoliefer.invoice.restcontroller;

import de.autoliefer.camunda.api.CamundaApi;
import de.autoliefer.camunda.enums.TaskType;
import de.autoliefer.invoice.dto.InvoiceDTO;
import de.autoliefer.invoice.dto.InvoiceMapper;
import de.autoliefer.invoice.persistence.InvoiceEntity;
import de.autoliefer.invoice.persistence.InvoiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.ResourceAccessException;

import java.util.Optional;

@RestController
public class InvoiceController {

    Logger logger = LoggerFactory.getLogger(InvoiceController.class);

    private InvoiceRepository invoiceRepository;
    private CamundaApi camundaApi;

    @Autowired
    public InvoiceController(InvoiceRepository invoiceRepository, CamundaApi camundaApi) {
        this.invoiceRepository = invoiceRepository;
        this.camundaApi = camundaApi;
    }

    @GetMapping(value = "/invoice/ordernr/{ordernr}")
    public InvoiceDTO getInvoiceByOrderNr(@PathVariable String ordernr) {
        InvoiceEntity invoiceEntity = invoiceRepository.findInvoiceEntityByOrderNr(ordernr);
        if (invoiceEntity != null) {
            InvoiceDTO invoiceDTO = InvoiceMapper.map(invoiceEntity);
            return invoiceDTO;
        } else {
            logger.info("Invoice with ordernr " + ordernr + " could not be found");
            return null;
        }
    }

    @PostMapping("/invoice/create")
    public ResponseEntity createInvoice(@RequestBody InvoiceDTO invoiceDTO) {
        InvoiceEntity invoiceEntity = InvoiceMapper.map(invoiceDTO);
        invoiceRepository.save(invoiceEntity);

        startCamundaProcess(invoiceDTO.getOwner(), invoiceDTO.getOrdernr());

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("invoice created successfully");
    }

    @GetMapping(value="/invoice/{id}")
    public InvoiceDTO findInvoice(@PathVariable Long id){
        Optional<InvoiceEntity> invoiceEntity = invoiceRepository.findById(id);
        if(invoiceEntity.isPresent()){
            return InvoiceMapper.map(invoiceEntity.get());
        }else{
            throw new ResourceAccessException("invoice with id " + id + " could not be found");
        }
    }

    @PutMapping(value="/invoice/update/{id}")
    public ResponseEntity updateInvoice(@PathVariable Long id, @RequestBody InvoiceDTO invoiceDTO){
        Optional<InvoiceEntity> invoiceOptional = invoiceRepository.findById(id);
        if(!invoiceOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }else{
            invoiceDTO.setId(id);
            InvoiceEntity invoiceEntity = InvoiceMapper.map(invoiceDTO);
            invoiceRepository.save(invoiceEntity);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body("invoice updated successfully");
        }
    }

    @DeleteMapping(value = "/invoice/delete/{id}")
    public ResponseEntity deleteInvoice(@PathVariable Long id) {
        invoiceRepository.deleteById(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body("invoice with id " + id + " deleted successfully");
    }

    private void startCamundaProcess(int owner, String ordernr){
        String processInstanceId;
        String taskId;
        String userId = String.valueOf(owner);

        processInstanceId = camundaApi.startProcessInstance(ordernr, TaskType.CREATE_INVOICE, owner);
        taskId = camundaApi.getFirstTaskIdByProcessInstanceId(processInstanceId);
        camundaApi.claimTask(taskId, userId);
        camundaApi.completeTask(taskId);
    }

}
