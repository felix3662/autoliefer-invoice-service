package de.autoliefer.invoice.persistence;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "invoice", schema = "public", catalog = "invoicedb")
public class InvoiceEntity {
    private long id;
    private String ordernr;
    private Date date;
    private double amount;
    private String annotation;
    private int owner;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY, generator="invoice_id_qwwseq")
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ordernr")
    public String getOrdernr() {
        return ordernr;
    }

    public void setOrdernr(String ordernr) {
        this.ordernr = ordernr;
    }

    @Basic
    @Column(name = "date")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "amount")
    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "annotation")
    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @Basic
    @Column(name = "owner")
    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvoiceEntity that = (InvoiceEntity) o;

        if (id != that.id) return false;
        if (Double.compare(that.amount, amount) != 0) return false;
        if (owner != that.owner) return false;
        if (ordernr != null ? !ordernr.equals(that.ordernr) : that.ordernr != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (annotation != null ? !annotation.equals(that.annotation) : that.annotation != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        long result;
        long temp;
        result = id;
        result = 31 * result + (ordernr != null ? ordernr.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (annotation != null ? annotation.hashCode() : 0);
        result = 31 * result + owner;
        return (int) result;
    }
}
