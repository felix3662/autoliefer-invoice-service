package de.autoliefer.invoice.persistence;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

// This is an Interface.
// No need Annotation here.
public interface InvoiceRepository extends CrudRepository<InvoiceEntity, Long> {

    @Query("select i from InvoiceEntity i where i.ordernr=?1")
    InvoiceEntity findInvoiceEntityByOrderNr(String ordernr);

}