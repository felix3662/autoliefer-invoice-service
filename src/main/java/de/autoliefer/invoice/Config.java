package de.autoliefer.invoice;

import de.autoliefer.camunda.api.CamundaApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("de.autoliefer")
public class Config {

    @Bean
    public CamundaApi camundaAPI() {
        return new CamundaApi();
    }

}