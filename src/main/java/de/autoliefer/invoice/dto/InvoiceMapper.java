package de.autoliefer.invoice.dto;

import de.autoliefer.invoice.persistence.InvoiceEntity;

public class InvoiceMapper {

    public static InvoiceDTO map(InvoiceEntity invoiceEntity){
        InvoiceDTO invoiceDTO = new InvoiceDTO();

        invoiceDTO.setId(invoiceEntity.getId());
        invoiceDTO.setOrdernr(invoiceEntity.getOrdernr());
        invoiceDTO.setDate(invoiceEntity.getDate());
        invoiceDTO.setAmount(invoiceEntity.getAmount());
        invoiceDTO.setAnnotation(invoiceEntity.getAnnotation());
        invoiceDTO.setOwner(invoiceEntity.getOwner());

        return invoiceDTO;
    }

    public static InvoiceEntity map(InvoiceDTO invoiceDTO){
        InvoiceEntity invoiceEntity = new InvoiceEntity();

        invoiceEntity.setId(invoiceDTO.getId());
        invoiceEntity.setOrdernr(invoiceDTO.getOrdernr());
        invoiceEntity.setDate(invoiceDTO.getDate());
        invoiceEntity.setAmount(invoiceDTO.getAmount());
        invoiceEntity.setAnnotation(invoiceDTO.getAnnotation());
        invoiceEntity.setOwner(invoiceDTO.getOwner());

        return invoiceEntity;
    }
}
